package behavior

import agents.HealthCareWorker;
import processes.Process
import repast.simphony.engine.schedule.ISchedulableAction;
import repast.simphony.engine.schedule.ScheduleParameters;
import structure.Room;


class RoomVisitDuration extends Process{
	HealthCareWorker healthCareWorker
	Room room
	ISchedulableAction leaveAction
	

	public RoomVisitDuration(HealthCareWorker hcw, Room r, double meanDuration) {
		super(meanDuration)
		healthCareWorker = hcw
		room = r
		
	}
	
	
	def start(){
		ScheduleParameters schedParams = ScheduleParameters.createOneTime((getNextEventTime()))
		leaveAction = schedule.schedule(schedParams, this, "leave")
		
	}
	
	def leave(){
		room.leave(healthCareWorker)
	}
	
	def stop(){
		
	}

}
