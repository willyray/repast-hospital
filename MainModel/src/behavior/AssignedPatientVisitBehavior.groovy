package behavior

import agents.HealthCareWorker
import agents.Patient
import mainModel.GranularBuilder
import processes.Process
import repast.simphony.engine.schedule.ISchedulableAction
import repast.simphony.engine.schedule.ScheduleParameters
import structure.Room
import repast.simphony.space.graph.Network
import repast.simphony.space.graph.RepastEdge

import java.util.Random

import org.apache.commons.math3.distribution.ExponentialDistribution
import org.apache.commons.math3.distribution.GammaDistribution

class AssignedPatientVisitBehavior extends Process {
	private HealthCareWorker hcw
	private ISchedulableAction visitAction
	private ISchedulableAction leaveAction
	private GranularBuilder main
	private Network<Object> assigned
	
	
	public AssignedPatientVisitBehavior(double intra, HealthCareWorker hcw, GranularBuilder main, Network<Object> assigned) {
		super(intra)
		this.main = main
		this.hcw = hcw
		this.assigned = assigned;
	}
	
	public visit() {
		
		if(assigned.getOutDegree(hcw) > 0) {
			Patient p = assigned.getRandomAdjacent(hcw);
			Room room = p.getRoom()
			room.visit(hcw, p)
			hcw.setInCommonArea(false)
			ExponentialDistribution leaveDistro = new ExponentialDistribution(0.01)
			double diff = leaveDistro.sample()
			ScheduleParameters schedParams = ScheduleParameters.createOneTime((schedule.getTickCount()+diff))
			main.event("VISIT", hcw.toString() + " to " + p.toString() + " in " + room.toString())
			leaveAction = schedule.schedule(schedParams, this, "leave", p, room, hcw)	
		} else {
			start()
		}
	}
	
	public leave(Patient p, Room r, HealthCareWorker hcw) {
		r.leave(hcw)
		hcw.setInCommonArea(true)
		main.event("LEAVE", hcw.toString() + " to " + p.toString() + " in " + r.toString())
		start()
	}

	@Override
	public Object start() {
		ScheduleParameters schedParams = ScheduleParameters.createOneTime((getNextEventTime()))
		visitAction = schedule.schedule(schedParams, this, "visit")
		
	}

	@Override
	public Object stop() {
		return null;
	}


}
