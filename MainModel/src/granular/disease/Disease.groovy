package granular.disease

import static granular.disease.Disease.State.RECOVERED

import java.time.ZonedDateTime

import actionevents.ColonizedEvent
import actionevents.ContaminationPulseEvent
import actionevents.DiseaseProgression
import agents.HealthCareWorker
import agents.Patient
import cern.jet.random.AbstractDistribution
import cern.jet.random.Uniform
import processes.Process
import repast.simphony.engine.schedule.ScheduleParameters
import repast.simphony.random.RandomHelper
import structure.Room
import structure.Surface
import structure.Zone
import util.TimeUtils

class Disease extends Process {
	
	enum State {
		SUSCEPTIBLE, COLONIZED, INFECTED, RECOVERED
	}
	
	
	enum PulseValues{
		PATIENT(21000),
		NEAR(17000),
		FAR(4200)
		
		int amount
		
		public PulseValues(int a) {
			this.amount = a
		}
	}
	
	public State state
	public Patient patient
	public boolean imported, nonProgressingImports, discharge
	public double timeOfColonization, timeOfProgression, timeOfDetection, timeOfRecovery
	
	public static final AbstractDistribution PROGRESS_DISTRO = RandomHelper.createUniform(3, 7)
	//TODO: Get a distribution from Mike
	
	public static final double pulseFrequency = 2.0*TimeUtils.HOUR
	
	public Disease(Patient p, State s, boolean i) {
		super(0)
		this.patient = p
		this.state = s
		this.imported = i
		this.discharge = false
		
		
		
		if (state == State.COLONIZED) {
			startProgressionTimer()
			startContaminationPulse()
			timeOfColonization = -1
		}
		
		if (state == State.INFECTED) {
			startContaminationPulse()
			startDetectionTimer()
			timeOfColonization = -1
			timeOfProgression = -1
		}
	}
	
	public colonize(double load) {
		if (state == State.SUSCEPTIBLE) {
			state = State.COLONIZED
			startProgressionTimer()
			startContaminationPulse()
			fireActionPerformed(new ColonizedEvent(patient, load))
		}
	}
	
	
	public startDetectionTimer() {
		
	}
	
	public startContaminationPulse() {
		if (discharge == true) {
			return
		}
		double nextTime = TimeUtils.getSchedule().getTickCount() + pulseFrequency
		ScheduleParameters schedParam = ScheduleParameters.createRepeating(TimeUtils.getSchedule().getTickCount()+pulseFrequency, pulseFrequency)
		patient.getBehaviors().put("contamination_pulse", (Object)TimeUtils.getSchedule().schedule(schedParam, this, "pulse"))
	}
	
	public pulse() {
		if (discharge == true) {
			return
		}
		if (state in [State.COLONIZED, State.INFECTED]) {
			Room r = patient.getRoom()
			ArrayList<Surface> near = r.getNearZone()
			ArrayList<Surface> far = r.getFarZone()
			Surface patSurf = patient.getProperty("surface")
			List<HealthCareWorker> visitors = r.hcwVisitors
			for (HealthCareWorker hcw in visitors) {
				near.add(hcw.getProperty("surface"))
			}
			
			double multiplier = 0
			if (state == State.COLONIZED) {
				multiplier = 0.1
			} else if (state==State.INFECTED) {
				multiplier = 1
			}
			
			double treatmentMultiplier = 1
			if (patient.hasProperty("treatment")) {
				double timeStart = patient.getProperty("treatmentstart")
				double treatmentDuration = patient.getProperty("treatmentduration")
				double treatmentSoFar = TimeUtils.getSchedule().getTickCount()-timeStart
				treatmentMultiplier = Math.max(0, treatmentSoFar/treatmentduration)
			}
			
			patSurf.addContam(PulseValues.PATIENT.amount*multiplier*treatmentMultiplier)
			int numNearOjbects = near.size()
			
			double nearTotalSurface = 0.0
			double farTotalSurface = 0.0
			for (Surface s : near) {
				nearTotalSurface += s.surfaceArea
			}
			for (Surface s : far) {
				farTotalSurface += s.surfaceArea
			}
			
			for (Surface s : near) {
				s.addContam(PulseValues.NEAR.amount*multiplier*(s.surfaceArea/nearTotalSurface)*treatmentMultiplier)
			}
			int numFarObjects = far.size()
			for (Surface s : far) {
				s.addContam(PulseValues.FAR.amount*multiplier*(s.surfaceArea/farTotalSurface)*treatmentMultiplier)
			} 
			
			
			
			
			fireActionPerformed(new ContaminationPulseEvent(patient))
			
			for (HealthCareWorker hcw in visitors) {
				near.remove(hcw.getProperty("surface"))
			}
		
		}
	}
	
	public startProgressionTimer() {
		if (state == State.COLONIZED && discharge == false) {
			double timeToProgression = PROGRESS_DISTRO.nextDouble()
			double timeOfProgression = TimeUtils.getSchedule().getTickCount() + timeToProgression
			ScheduleParameters schedParams = ScheduleParameters.createOneTime(timeOfProgression)
			TimeUtils.getSchedule().schedule(schedParams, this, "progression")
		}
	}
	
	
	public void progression() {
		if (state == State.COLONIZED && imported == false) {
			state = State.INFECTED
			patient.fireActionPerformed(new DiseaseProgression(patient))
			double current = TimeUtils.getSchedule().getTickCount()
			double timeToDetect = current + RandomHelper.createUniform(0.5, 1.5).nextDouble()
			ScheduleParameters schedParams = ScheduleParameters.createOneTime(timeToDetect)
			TimeUtils.getSchedule().schedule(schedParams, this, "detection")
		}
	}
	
	
	public void detection() {
		patient.setProperty("detected", true)
		patient.setProperty("isolated", true)
		patient.setProperty("treatment", true)
		patient.setProperty("treatmentstart", TimeUtils.getSchedule().getTickCount())
		double treatmentDuration = 5
		patient.setProperty("treatmentduration", treatmentDuration)
		double treatmentStopsAt = TimeUtils.getSchedule().getTickCount()
		patient.setProperty("treatmentStop", treatmentStopsAt)
		ScheduleParameters schedParams = ScheduleParameters.createOneTime(treatmentStopsAt)
		TimeUtils.getSchedule().schedule(schedParams, this, "treatmentStop")
		
	}
	
	public treatmentStop() {
		patient.setProperty("treatment", false)
		state = RECOVERED
	}

	@Override
	public Object start() {
	
	}

	@Override
	public Object stop() {
		discharge = true
	}
	
	
}
