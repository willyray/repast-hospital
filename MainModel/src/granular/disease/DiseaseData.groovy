package granular.disease

import agents.Patient
import granular.behavior.GranularContactMan
import mainModel.GranularBuilder

class DiseaseData {
	DiseaseManager diseaseMan
	GranularBuilder main
	
	
	
	
	public DiseaseData(DiseaseManager diseaseMan, GranularBuilder main) {
		this.diseaseMan = diseaseMan
		this.main=main
	}
	
	public double getColonizationPrevalence() {
		double census = main.patients.size()
		double count = 0.0
		for (Patient p in main.patients) {
			if (p.getProperty("disease") != null) {
				if (p.getProperty("disease").state == Disease.State.COLONIZED) {
					count += 1.0
				}
			}
		}
		double prevalence = count/census
		println(prevalence)
		return prevalence
	}
	
}
