package granular.disease

import static org.junit.Assert.assertTrue

import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import actionevents.*
import util.Chooser
import agents.*
import granular.behavior.HandHygieneManager
import granular.disease.Disease
import mainModel.GranularBuilder
import processes.Process
import repast.simphony.engine.environment.RunEnvironment
import repast.simphony.engine.schedule.ScheduleParameters
import repast.simphony.parameter.Parameters
import structure.Room
import structure.Surface
import util.TimeUtils
import java.lang.Math

import org.apache.tools.ant.types.Environment

class DiseaseManager extends Process implements ActionListener{
	
	double importationRate = 0.075
	ArrayList<Disease.State> importStates = [Disease.State.COLONIZED, Disease.State.INFECTED, Disease.State.RECOVERED]
	ArrayList<Double> importProbabilities = [0.85, 0.1, 0.05]
	int importationCount = 0
	GranularBuilder main
	
	double gelEfficacy, soapEfficacy
	
	
	public DiseaseManager(GranularBuilder main) {
		super(0)
		this.importationRate = 0.075
		this.main = main
		
		ScheduleParameters schedParams = ScheduleParameters.createRepeating(1, 15*TimeUtils.MINUTE)
		TimeUtils.getSchedule().schedule(schedParams, this, "patientSelfColonizeCheck")
	}
	
	
	public cleanDisease(Room r) {
		double cleaningEfficacy = RunEnvironment.getInstance().getParameters().getDouble("roomCleaningEfficacy")
		for (Surface s in r.getNearZone() + r.getFarZone()) {
			double currentLoad = s.getLoad()
			double reduction = currentLoad*cleaningEfficacy
			s.addContam(-reduction)
		}
		if (r.getPatients().size() > 0) {
			Patient p = r.getPatients()[0]
			double currentLoad = p.getProperty("surface").getLoad()
			double reduction = currentLoad*cleaningEfficacy
			p.getProperty("surface").addContam(-reduction)
		}
		
	}
	
	

	
	
	public patientSelfColonizeCheck() {
		for (Patient p  in main.patients) {
			double load = p.surface.getLoad()
			if (p.disease == null && load > 0) {
				Parameters params = RunEnvironment.getInstance().getParameters()
				double c = params.getDouble("susceptibility")
				double probabilityOfColonization = 1-Math.exp(-1*c*load)
				
				try {
				if (Chooser.randomTrue(probabilityOfColonization)) {
					selfColonize(p, load)
				}
				}
				 catch (Exception e) {
					println("probability of self colonize NaN")
				
				}
				
			}
		}
	}
	
	
	public selfColonize(Patient p, double load) {
		if (p.getProperty("disease") == null) {
			Disease disease = new Disease(p, Disease.State.SUSCEPTIBLE,false)
			p.setProperty("disease", disease)
			disease.addActionListener(this)
			disease.colonize(load)
		}

	}
	
	
	public checkImportation(Patient p) {
		boolean importation = Chooser.randomTrue(importationRate)
		return importation
	}
	
	def importOn(Patient p) {
		Disease.State importState = Chooser.choose(importStates, importProbabilities)
		Disease disease = new Disease(p, importState, false)
		p.setProperty("disease", disease)
		disease.addActionListener(this)
		p.setProperty("imported", true)

		
	}
	
	def washHands(HealthCareWorker hcw, HandHygieneManager.Types type) {
		double load = hcw.getProperty("surface").getLoad()
		double efficacy = 0
		if (type == HandHygieneManager.Types.GEL) {
			efficacy = 0.6
		} else if (type == HandHygieneManager.Types.SOAP) {
			efficacy = 0.9
		}
		
		double reduction = load * efficacy
		hcw.getProperty("surface").addContam(-1*reduction)
		
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e instanceof PatientAdmitted) {
			PatientAdmitted pa = (PatientAdmitted) e
			Patient p = pa.getPatient()
			if (checkImportation(p)) {
				fireActionPerformed(new ImportationEvent(p))
				importOn(p)
			}
			
		}
		
		if (e instanceof ColonizedEvent) {
			fireActionPerformed(e)
		}
		
		if (e instanceof DiseaseProgression) {
			fireActionPerformed(e)
		}
		
		if (e instanceof ContaminationPulseEvent) {
			fireActionPerformed(e)
		}
		
		if (e instanceof RoomCleaningEvent) {
			RoomCleaningEvent rce = (RoomCleaningEvent)e
			cleanDisease(rce.room)
		}
		
		if (e instanceof WashHandsEvent) {
			WashHandsEvent whe = (WashHandsEvent)e
			washHands(whe.getHcw(), whe.hhType)
		}
	}
	
	
	
	@Override
	public Object start() {
		return null;
	}

	@Override
	public Object stop() {
		return null;
	}
}
