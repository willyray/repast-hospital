package granular.behavior;

import cern.jet.random.Poisson;
import repast.simphony.random.RandomHelper;

public enum VisitType {
	
	NONEAR_FEW_SHORT(RandomHelper.createPoisson(0.044),
			RandomHelper.createPoisson(0.048)),
	NONEAR_FEW_LONG(RandomHelper.createPoisson(0.114), 
			RandomHelper.createPoisson(0.097)),
	NONEAR_LOTS_SHORT(RandomHelper.createPoisson(0.580),
			RandomHelper.createPoisson(1.983)),
	NONEAR_LOTS_LONG(RandomHelper.createPoisson(1.604),
			RandomHelper.createPoisson(2.919)),
	NEAR_FEW_SHORT(RandomHelper.createPoisson(0.051),
			RandomHelper.createPoisson(0.031)),
	NEAR_FEW_LONG(RandomHelper.createPoisson(0.066),
			RandomHelper.createPoisson(0.037)),
	NEAR_LOTS_SHORT(RandomHelper.createPoisson(0.528),
			RandomHelper.createPoisson(0.860)),
	NEAR_LOTS_LONG(RandomHelper.createPoisson(1.158),
			RandomHelper.createPoisson(1.745));
	
	public final Poisson near;
	public final Poisson far;
	
	private VisitType(Poisson near, Poisson far) {
		this.near = near;
		this.far = far;
	}
	
}
