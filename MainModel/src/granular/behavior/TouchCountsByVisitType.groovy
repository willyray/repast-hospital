package granular.behavior

import cern.jet.random.NegativeBinomial
import cern.jet.random.Poisson
import granular.behavior.VisitType
import repast.simphony.random.RandomHelper

class TouchCountsByVisitType {
	
	public static final Poisson NONEAR_FEw_SHORT_NEAR = RandomHelper.createPoisson(0.044)
	public static final Poisson NONEAR_FEW_SHORT_FAR = RandomHelper.createPoisson(0.048)
	
	public static final Poisson NONEAR_FEW_LONG_NEAR = RandomHelper.createPoisson(0.114)
	public static final Poisson NONEAR_FEW_LONG_FAR = RandomHelper.createPoisson(0.097)
	
	public static final Poisson NONEAR_LOTS_SHORT_NEAR = RandomHelper.createPoisson(0.580)
	public static final Poisson NONEAR_LOTS_SHORT_FAR = RandomHelper.createPoisson(1.983)
	
	public static final Poisson NONEAR_LOTS_LONG_NEAR = RandomHelper.createPoisson(1.604)
	public static final Poisson NONEAR_LOGS_LONG_FAR = RandomHelper.createPoisson(2.919)
	
	public static final Poisson NEAR_FEW_SHORT_NEAR = RandomHelper.createPoisson(0.051)
	public static final Poisson NEAR_FEW_SHORT_FAR = RandomHelper.createPoisson(0.031)
	
	public static final Poisson NEAR_FEW_LONG_NEAR = RandomHelper.createPoisson(0.066)
	public static final Poisson NEAR_FEW_LONG_FAR = RandomHelper.createPoisson(0.037)
	
	public static final Poisson NEAR_LOTS_SHORT_NEAR = RandomHelper.createPoisson(0.528)
	public static final Poisson NEAR_LOTS_SHORT_FAR = RandomHelper.createPoisson(0.860)
	
	public static final Poisson NEAR_LOTS_LONG_NEAR = RandomHelper.createPoisson(1.158)
	public static final Poisson NEAR_LOTS_LONG_FAR = RandomHelper.createPoisson(1.745)
	
}
	