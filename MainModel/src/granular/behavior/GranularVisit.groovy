package granular.behavior

import java.awt.event.ActionEvent
import java.security.PrivateKey
import java.util.concurrent.ScheduledExecutorService

import org.apache.commons.math3.distribution.AbstractRealDistribution
import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.apache.commons.math3.distribution.GammaDistribution

import actionevents.ContaminationTransferEvent
import actionevents.RoomLeave
import actionevents.RoomVisit
import actionevents.TouchSurfaceAction
import actionevents.WashHandsEvent
import agents.AgentType
import agents.HealthCareWorker
import agents.Patient
import cern.jet.random.Gamma
import mainModel.GranularBuilder
import processes.Process
import repast.simphony.engine.environment.RunEnvironment
import repast.simphony.engine.schedule.ISchedulableAction
import repast.simphony.engine.schedule.ScheduleParameters
import repast.simphony.parameter.Parameters
import repast.simphony.space.graph.Network
import structure.Room
import structure.Surface
import util.Chooser
import repast.simphony.random.RandomHelper
import granular.behavior.VisitType


class GranularVisit extends Process{

	private Gamma distribution
	private HealthCareWorker hcw
	private AgentType agentType
	private Network<Object> assigned
	private boolean assignedVisit
	private ISchedulableAction visitAction, leaveAction
	private ArrayList<Double> visitTypeProbs = new ArrayList<Double>()
	private ArrayList<VisitType> visitTypes
	private HashMap<VisitType, Gamma> visitDurations
	private GranularBuilder main
	private HandHygieneManager handHygieneMan
	
	public static final double MINUTES = 1.0/60.0/24.0;




	def GranularVisit (Gamma distro
	, HealthCareWorker hcw
	, Network<Object> assigned
	, Boolean assignedVisit
	, HashMap<VisitType, Gamma> visitDurations
	, GranularBuilder main){

		super(1)
		this.distribution = distro;
		this.hcw = hcw;
		this.agentType = hcw.type
		this.assignedVisit = assignedVisit
		this.visitDurations = visitDurations
		this.main = main
		this.assigned = assigned
		this.handHygieneMan = new HandHygieneManager()
		
		VisitTypeProbsByRole vtpbr = new VisitTypeProbsByRole()

		if (this.agentType == AgentType.NURSE) {
			if (assignedVisit) {
				this.visitTypeProbs = vtpbr.PROBS['NURSE']
			} else {
				this.visitTypeProbs = vtpbr.PROBS['NURSE_RANDOM']
			}
		}
		else {
			visitTypeProbs = vtpbr.PROBS[this.agentType.value]
		}


		visitTypes = new ArrayList<VisitType>(
				Arrays.asList(VisitType.NONEAR_FEW_SHORT,
				VisitType.NONEAR_FEW_LONG,
				VisitType.NONEAR_LOTS_SHORT,
				VisitType.NONEAR_LOTS_LONG,
				VisitType.NEAR_FEW_SHORT,
				VisitType.NEAR_FEW_LONG,
				VisitType.NEAR_LOTS_SHORT,
				VisitType.NEAR_LOTS_LONG)
				)
	}



	public Object start() {
		double tte = distro.sample()*MINUTES
		double current = schedule.getTickCount()

		ScheduleParameters schedParams = ScheduleParameters.createOneTime(tte+current)
		if (assignedVisit) {
			visitAction = schedule.schedule(schedParams, this, "assignedRoomVisit")
		} else {
			visitAction = schedule.schedule(schedParams, this, "visit")
		}
	}

	public Object stop() {
		return null;
	}

	public visit() {
		if (main.patients.size() > 0 && hcw.inCommonArea==true) {
			Patient p = Chooser.chooseOne(main.patients)
			VisitType thisVisitType = Chooser.choose(visitTypes,visitTypeProbs)
			def elapsedDuration = visitDurations.get(thisVisitType).nextDouble()*MINUTES
			def timeOfDeparture = schedule.getTickCount() + elapsedDuration
			Room room = p.getRoom()
			room.visit(hcw, p)
			ScheduleParameters schedulParams = ScheduleParameters.createOneTime(timeOfDeparture)
			leaveAction = schedule.schedule(schedulParams, this, "leave", p, room, hcw, thisVisitType )
			
			int near_touches =thisVisitType.near.nextInt()
			int far_touches = thisVisitType.far.nextInt()
			int patient_touches = thisVisitType.near.nextInt()
			fireActionPerformed(new RoomVisit(hcw, room, p, thisVisitType, "visit") )
			if (handHygieneMan.checkHandHygeine(thisVisitType, hcw, room, p, true)){
				HandHygieneManager.Types type = handHygieneMan.getType(thisVisitType, hcw, room, p, true)
				fireActionPerformed(new WashHandsEvent(hcw, type))
			}
			main.event("RANDOM_VISIT", hcw.toString() + " to " + p.toString() + " in " + room.toString() + " (" + thisVisitType.toString() +  " " + near_touches + "n/" + far_touches + "f)")
			hcw.setInCommonArea(false)
			createTouches(near_touches, far_touches, patient_touches, elapsedDuration, room, p)
			
		} else {
			start()
		}
	}
	
	public washHands() {
		
	}

	public assignedRoomVisit() {
		
		if (assigned != null && assigned.getOutDegree(hcw)>0 && hcw.inCommonArea==true) {
			VisitType thisVisitType = Chooser.choose(visitTypes, visitTypeProbs)
			def elapsedDuration = visitDurations.get(thisVisitType).nextDouble()*MINUTES
			def timeOfDeparture = schedule.getTickCount()+elapsedDuration
			
			Patient p = assigned.getRandomAdjacent(hcw)
			Room room = p.getRoom()
			ScheduleParameters schedParams = ScheduleParameters.createOneTime(timeOfDeparture)
			leaveAction = schedule.schedule(schedParams, this, "leave", p, room, hcw, thisVisitType)
			room.visit(hcw, p)
			
			int near_touches =thisVisitType.near.nextInt()
			int far_touches = thisVisitType.far.nextInt()
			int patient_touches = thisVisitType.near.nextInt()
			
			main.event("VISIT", hcw.toString() + " to " + p.toString() + " in " + room.toString() + " (" + thisVisitType.toString() +  " " + near_touches + "n/" + far_touches + "f)")
			hcw.setInCommonArea(false)
			
			
			fireActionPerformed(new RoomVisit(hcw, room, p, thisVisitType, "visit") )
			if (handHygieneMan.checkHandHygeine(thisVisitType, hcw, room, p, true)){
				HandHygieneManager.Types type = handHygieneMan.getType(thisVisitType, hcw, room, p, true)
				fireActionPerformed(new WashHandsEvent(hcw, type))
			}

			createTouches(near_touches, far_touches, patient_touches, elapsedDuration, room, p)
			

		} else {
			start()
		}
	}
	
	private createTouches(int near, int far, int patient_touches, double duration, Room room, Patient patient) {
		int n = near
		int f = far
		int p = patient_touches
		int total = n+f+p
		double intratouchtime = duration/total
		double current = schedule.getTickCount()
		ArrayList<String> touches = new ArrayList<String>()
		for (int i=0; i<total; i++) {
			if (i < near) {
				touches.add("n")
			} else if (i < n+f){
				touches.add("f")
			} else {
				touches.add("p")
			}
		}
		Collections.shuffle(touches)
		for (int i = 0; i<touches.size(); i++ ) {
			double next = current + (intratouchtime*i)
			ScheduleParameters schedParams = ScheduleParameters.createOneTime(next)
			String zone = touches.get(i)
			ISchedulableAction next_touch = schedule.schedule(schedParams, this, "touch", room, zone, patient)
		}
		
	}
	
	public touch(Room room, String zone, Patient patient) {
		Surface target = null; 
		if (zone == "n") {
			target = Chooser.chooseOne(room.getNearZone())
		} else if (zone == "f"){
			target = Chooser.chooseOne(room.getFarZone())
		} else {
			target = patient.getProperty("surface")
		}
		
		if (target.getName().contains("DOCTOR") || target.getName().contains("NURSE")) {
			for (int i = 0; i<1; i++) {
				
			}
		}
		
		main.event("TOUCH", hcw.toString() + " touching " + target.getName())
		Surface hcwSurface = hcw.getProperty("surface")
		fireActionPerformed(new TouchSurfaceAction(hcwSurface, target))
		transferContamination(hcwSurface, target)
		
	}
	
	public transferContamination(Surface toucher, Surface touchee) {
		Parameters params = RunEnvironment.getInstance().getParameters()
		double surfaceLoad = touchee.getLoad()
		double toucherLoad = toucher.getLoad()
		double surfaceTransferFraction = params.getDouble("surfaceTransferFraction")
		double toHand = surfaceTransferFraction * ((surfaceLoad * toucher.surfaceArea / touchee.surfaceArea) - (toucherLoad/2.0))
		toucher.addContam(toHand)
		touchee.addContam(-1*toHand)
		ContaminationTransferEvent cte = new ContaminationTransferEvent(toucher.getName(), touchee.getName(), toHand)
		fireActionPerformed(cte)
		main.event("CONTAM_TRANSFER", cte.toString())
		
		
		
	}
/*
	getRandomNetTransferredToHand = function(surfaceTransferFraction, surfaceArea, handContam, surfaceContam){
		return(surfaceTransferFraction * (surfaceContam * handArea / surfaceArea - handContam / 2))
}

netSurfaceToHand = getRandomNetTransferredToHand(surface.transferFraction, surface.area,  person.handContam, surface.contam)

person.handContam = person.handContam + netSurfaceToHand
surface.contam = surface.contam - netSurfacetoHand
*/

	public leave(Patient p, Room r, HealthCareWorker hcw, VisitType vtype) {
		r.leave(hcw)
		if (handHygieneMan.checkHandHygeine(thisVisitType, hcw, room, p, true)){
			HandHygieneManager.Types type = handHygieneMan.getType(thisVisitType, hcw, room, p, true)
			fireActionPerformed(new WashHandsEvent(hcw, type))
		}
		fireActionPerformed(new RoomLeave(hcw, r, "leave") )
		
		hcw.setInCommonArea(true)
		//todo: move this main call to the action performed on granularbuilder
		main.event("LEAVE", hcw.toString() + " to " + p.toString() + " in " + r.toString())
		start()
	}
}
