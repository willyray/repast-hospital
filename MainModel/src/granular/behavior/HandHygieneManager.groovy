package granular.behavior

import javax.swing.event.DocumentEvent.EventType

import actionevents.RoomVisit
import agents.AgentType
import agents.HealthCareWorker
import agents.Patient
import structure.Room
import util.Chooser
import granular.behavior.VisitType

class HandHygieneManager {

	def NURSE = [
		'BEFORE' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.16,
				(VisitType.NONEAR_FEW_LONG) : 0.20,
				(VisitType.NONEAR_LOTS_SHORT) : 0.65,
				(VisitType.NONEAR_LOTS_LONG) : 0.69,
				(VisitType.NEAR_FEW_SHORT) : 0.19,
				(VisitType.NEAR_FEW_LONG) : 0.21,
				(VisitType.NEAR_LOTS_SHORT) : 0.62,
				(VisitType.NEAR_LOTS_LONG) : 0.68	
			],
		'AFTER' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.15,
				(VisitType.NONEAR_FEW_LONG) : 0.20,
				(VisitType.NONEAR_LOTS_SHORT) : 0.61,
				(VisitType.NONEAR_LOTS_LONG) : 0.70,
				(VisitType.NEAR_FEW_SHORT) : 0.21,
				(VisitType.NEAR_FEW_LONG) : 0.21,
				(VisitType.NEAR_LOTS_SHORT) : 0.68,
				(VisitType.NEAR_LOTS_LONG) : 0.67
			],		
		
		'BEFORE_ISOLATION' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.17,
				(VisitType.NONEAR_FEW_LONG) : 0.27,
				(VisitType.NONEAR_LOTS_SHORT) : 0.67,
				(VisitType.NONEAR_LOTS_LONG) : 0.55,
				(VisitType.NEAR_FEW_SHORT) : 0.25,
				(VisitType.NEAR_FEW_LONG) : 0.23,
				(VisitType.NEAR_LOTS_SHORT) : 0.54,
				(VisitType.NEAR_LOTS_LONG) : 0.67
			],
		'AFTER_ISOLATION' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.18,
				(VisitType.NONEAR_FEW_LONG) : 0.24,
				(VisitType.NONEAR_LOTS_SHORT) : 0.67,
				(VisitType.NONEAR_LOTS_LONG) : 0.60,
				(VisitType.NEAR_FEW_SHORT) : 0.31,
				(VisitType.NEAR_FEW_LONG) : 0.32,
				(VisitType.NEAR_LOTS_SHORT) : 0.58,
				(VisitType.NEAR_LOTS_LONG) : 0.69
			]
		
		]
		
	def NURSE_FLOAT = [
		'BEFORE' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.15,
				(VisitType.NONEAR_FEW_LONG) : 0.09,
				(VisitType.NONEAR_LOTS_SHORT) : 1.0,
				(VisitType.NONEAR_LOTS_LONG) : 0.56,
				(VisitType.NEAR_FEW_SHORT) : 0.17,
				(VisitType.NEAR_FEW_LONG) : 0.26,
				(VisitType.NEAR_LOTS_SHORT) : 0.56,
				(VisitType.NEAR_LOTS_LONG) : 0.78
			],
		'AFTER' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.13,
				(VisitType.NONEAR_FEW_LONG) : 0.14,
				(VisitType.NONEAR_LOTS_SHORT) : 1.0,
				(VisitType.NONEAR_LOTS_LONG) : 0.63,
				(VisitType.NEAR_FEW_SHORT) : 0.15,
				(VisitType.NEAR_FEW_LONG) : 0.25,
				(VisitType.NEAR_LOTS_SHORT) : 0.56,
				(VisitType.NEAR_LOTS_LONG) : 0.73
			],
		
		'BEFORE_ISOLATION' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.11,
				(VisitType.NONEAR_FEW_LONG) : 0.0,
				(VisitType.NONEAR_LOTS_SHORT) : 1.0,
				(VisitType.NONEAR_LOTS_LONG) : 0.33,
				(VisitType.NEAR_FEW_SHORT) : 0.0,
				(VisitType.NEAR_FEW_LONG) : 0.22,
				(VisitType.NEAR_LOTS_SHORT) : 0.56,
				(VisitType.NEAR_LOTS_LONG) : 0.4
			],
		'AFTER_ISOLATION' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.13,
				(VisitType.NONEAR_FEW_LONG) : 0.07,
				(VisitType.NONEAR_LOTS_SHORT) : 1.0,
				(VisitType.NONEAR_LOTS_LONG) : 0.67,
				(VisitType.NEAR_FEW_SHORT) : 0.10,
				(VisitType.NEAR_FEW_LONG) : 0.0,
				(VisitType.NEAR_LOTS_SHORT) : 0.56,
				(VisitType.NEAR_LOTS_LONG) : 0.50
			]
		
		]

	def CNA = [
		'BEFORE' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.11,
				(VisitType.NONEAR_FEW_LONG) : 0.15,
				(VisitType.NONEAR_LOTS_SHORT) : 0.54,
				(VisitType.NONEAR_LOTS_LONG) : 0.71,
				(VisitType.NEAR_FEW_SHORT) : 0.15,
				(VisitType.NEAR_FEW_LONG) : 0.15,
				(VisitType.NEAR_LOTS_SHORT) : 0.59,
				(VisitType.NEAR_LOTS_LONG) : 0.66
			],
		'AFTER' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.11,
				(VisitType.NONEAR_FEW_LONG) : 0.14,
				(VisitType.NONEAR_LOTS_SHORT) : 0.51,
				(VisitType.NONEAR_LOTS_LONG) : 0.74,
				(VisitType.NEAR_FEW_SHORT) : 0.14,
				(VisitType.NEAR_FEW_LONG) : 0.13,
				(VisitType.NEAR_LOTS_SHORT) : 0.57,
				(VisitType.NEAR_LOTS_LONG) : 0.68
			],
		
		'BEFORE_ISOLATION' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.09,
				(VisitType.NONEAR_FEW_LONG) : 0.21,
				(VisitType.NONEAR_LOTS_SHORT) : 1.0,
				(VisitType.NONEAR_LOTS_LONG) : 0.81,
				(VisitType.NEAR_FEW_SHORT) : 0.23,
				(VisitType.NEAR_FEW_LONG) : 0.13,
				(VisitType.NEAR_LOTS_SHORT) : 0.5,
				(VisitType.NEAR_LOTS_LONG) : 0.38
			],
		'AFTER_ISOLATION' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.11,
				(VisitType.NONEAR_FEW_LONG) : 0.23,
				(VisitType.NONEAR_LOTS_SHORT) : 1.0,
				(VisitType.NONEAR_LOTS_LONG) : 0.81,
				(VisitType.NEAR_FEW_SHORT) : 0.30,
				(VisitType.NEAR_FEW_LONG) : 0.25,
				(VisitType.NEAR_LOTS_SHORT) : 0.50,
				(VisitType.NEAR_LOTS_LONG) : 0.54
			]
		
		]

	
	
	def DOCTOR = [
		'BEFORE' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.1,
				(VisitType.NONEAR_FEW_LONG) : 0.11,
				(VisitType.NONEAR_LOTS_SHORT) : 0.5,
				(VisitType.NONEAR_LOTS_LONG) : 1.0,
				(VisitType.NEAR_FEW_SHORT) : 0.14,
				(VisitType.NEAR_FEW_LONG) : 0.2,
				(VisitType.NEAR_LOTS_SHORT) : 0.86,
				(VisitType.NEAR_LOTS_LONG) : 0.63
			],
		'AFTER' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.1,
				(VisitType.NONEAR_FEW_LONG) : 0.11,
				(VisitType.NONEAR_LOTS_SHORT) : 0.5,
				(VisitType.NONEAR_LOTS_LONG) : 1.0,
				(VisitType.NEAR_FEW_SHORT) : 0.12,
				(VisitType.NEAR_FEW_LONG) : 0.22,
				(VisitType.NEAR_LOTS_SHORT) : 0.71,
				(VisitType.NEAR_LOTS_LONG) : 0.56
			],
		
		'BEFORE_ISOLATION' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.1,
				(VisitType.NONEAR_FEW_LONG) : 0.11,
				(VisitType.NONEAR_LOTS_SHORT) : 1.0,
				(VisitType.NONEAR_LOTS_LONG) : 1.0,
				(VisitType.NEAR_FEW_SHORT) : 0.12,
				(VisitType.NEAR_FEW_LONG) : 0.24,
				(VisitType.NEAR_LOTS_SHORT) : 0.67,
				(VisitType.NEAR_LOTS_LONG) : 0.38
			],
		'AFTER_ISOLATION' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.09,
				(VisitType.NONEAR_FEW_LONG) : 0.15,
				(VisitType.NONEAR_LOTS_SHORT) : 1.0,
				(VisitType.NONEAR_LOTS_LONG) : 1.0,
				(VisitType.NEAR_FEW_SHORT) : 0.16,
				(VisitType.NEAR_FEW_LONG) : 0.21,
				(VisitType.NEAR_LOTS_SHORT) : 0.67,
				(VisitType.NEAR_LOTS_LONG) : 0.38
			]
		
		]
	
		
		
	def PT = [
		'BEFORE' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.09,
				(VisitType.NONEAR_FEW_LONG) : 0.31,
				(VisitType.NONEAR_LOTS_SHORT) : 1.0,
				(VisitType.NONEAR_LOTS_LONG) : 0.95,
				(VisitType.NEAR_FEW_SHORT) : 0.10,
				(VisitType.NEAR_FEW_LONG) : 0.22,
				(VisitType.NEAR_LOTS_SHORT) : 0.75,
				(VisitType.NEAR_LOTS_LONG) : 0.80
			],
		'AFTER' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.14,
				(VisitType.NONEAR_FEW_LONG) : 0.31,
				(VisitType.NONEAR_LOTS_SHORT) : 1.0,
				(VisitType.NONEAR_LOTS_LONG) : 0.95,
				(VisitType.NEAR_FEW_SHORT) : 0.18,
				(VisitType.NEAR_FEW_LONG) : 0.18,
				(VisitType.NEAR_LOTS_SHORT) : 0.75,
				(VisitType.NEAR_LOTS_LONG) : 0.80
			],
		
		'BEFORE_ISOLATION' : [
				(VisitType.NONEAR_FEW_SHORT) : 0.33,
				(VisitType.NONEAR_FEW_LONG) : 0.31,
				(VisitType.NONEAR_LOTS_SHORT) : 1.0,
				(VisitType.NONEAR_LOTS_LONG) : 0.75,
				(VisitType.NEAR_FEW_SHORT) : 1.0,
				(VisitType.NEAR_FEW_LONG) : 0.11,
				(VisitType.NEAR_LOTS_SHORT) : 1.0,
				(VisitType.NEAR_LOTS_LONG) : 1.0
			],
		'AFTER_ISOLATION' : [
					(VisitType.NONEAR_FEW_SHORT) : 0.33,
					(VisitType.NONEAR_FEW_LONG) : 0.25,
					(VisitType.NONEAR_LOTS_SHORT) : 1.0,
					(VisitType.NONEAR_LOTS_LONG) : 0.75,
					(VisitType.NEAR_FEW_SHORT) : 1.0,
					(VisitType.NEAR_FEW_LONG) : 0.33,
					(VisitType.NEAR_LOTS_SHORT) : 1.0,
					(VisitType.NEAR_LOTS_LONG) :1.0
				]
			
			]
			
		def RT = [
			'BEFORE' : [
					(VisitType.NONEAR_FEW_SHORT) : 0.17,
					(VisitType.NONEAR_FEW_LONG) : 0.2,
					(VisitType.NONEAR_LOTS_SHORT) : 1.0,
					(VisitType.NONEAR_LOTS_LONG) : 0.71,
					(VisitType.NEAR_FEW_SHORT) : 0.24,
					(VisitType.NEAR_FEW_LONG) : 0.24,
					(VisitType.NEAR_LOTS_SHORT) : 0.88,
					(VisitType.NEAR_LOTS_LONG) : 0.73
				],
			'AFTER' : [
					(VisitType.NONEAR_FEW_SHORT) : 0.19,
					(VisitType.NONEAR_FEW_LONG) : 0.26,
					(VisitType.NONEAR_LOTS_SHORT) : 1.0,
					(VisitType.NONEAR_LOTS_LONG) : 0.71,
					(VisitType.NEAR_FEW_SHORT) : 0.19,
					(VisitType.NEAR_FEW_LONG) : 0.28,
					(VisitType.NEAR_LOTS_SHORT) : 0.88,
					(VisitType.NEAR_LOTS_LONG) : 0.73
				],
			
			'BEFORE_ISOLATION' : [
					(VisitType.NONEAR_FEW_SHORT) : 0.16,
					(VisitType.NONEAR_FEW_LONG) : 0.34,
					(VisitType.NONEAR_LOTS_SHORT) : 1.0,
					(VisitType.NONEAR_LOTS_LONG) : 1.0,
					(VisitType.NEAR_FEW_SHORT) : 0.25,
					(VisitType.NEAR_FEW_LONG) : 0.27,
					(VisitType.NEAR_LOTS_SHORT) : 1.0,
					(VisitType.NEAR_LOTS_LONG) : 0.67
				],
			'AFTER_ISOLATION' : [
						(VisitType.NONEAR_FEW_SHORT) : 0.08,
						(VisitType.NONEAR_FEW_LONG) : 0.14,
						(VisitType.NONEAR_LOTS_SHORT) : 1.0,
						(VisitType.NONEAR_LOTS_LONG) : 0.5,
						(VisitType.NEAR_FEW_SHORT) : 0.05,
						(VisitType.NEAR_FEW_LONG) : 0.35,
						(VisitType.NEAR_LOTS_SHORT) : 1.0,
						(VisitType.NEAR_LOTS_LONG) : 0.67
					]
				
				]
				
				
	def HAND_HYGIENE_ADHERENCE = [
			(AgentType.NURSE) : NURSE,
			(AgentType.NURSE_FLOAT) : NURSE_FLOAT,
			(AgentType.CNA) : CNA,
			(AgentType.DOCTOR) : DOCTOR,
			(AgentType.PT) : PT,
			(AgentType.RT) : RT
		]
	
 
	
	enum Types {
		GEL,SOAP
	}
	
	public HandHygieneManager() {
	
	}
	
	
	
	public checkHandHygeine(VisitType eventType, HealthCareWorker hcw, Room room, Patient p, boolean beforeVisit ) {
		if (p.getProperty("isolated")==null || p.getProperty("isolated") == false) {
			if (beforeVisit) {
				return HAND_HYGIENE_ADHERENCE[hcw.type]['BEFORE'][eventType]
				
			} else {
				return HAND_HYGIENE_ADHERENCE[hcw.type]['AFTER'][eventType]
			}
		}
		
		if (p.getProperty("isolated")) {
			if (beforeVisit) {
				return HAND_HYGIENE_ADHERENCE[hcw.type]['BEFORE_ISOLATION'][eventType]
			} else {
				return HAND_HYGIENE_ADHERENCE[hcw.type]['AFTER_ISOLATION'][eventType]
				
			}
		}
	}
	
	public getType(VisitType eventType, HealthCareWorker hcw, Room room, Patient p, boolean beforeVisit) {
		def gel_fraction = 0.9
		
		if (p.getProperty("isolation")){
			gel_fraction = 0.1
		}

		return Chooser.choose([Types.GEL, Types.SOAP], [gel_fraction, 1-gel_fraction])
		
		
	}
	
	
	
}
