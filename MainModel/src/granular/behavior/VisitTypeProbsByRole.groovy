package granular.behavior

import agents.AgentType
import util.Chooser

class VisitTypeProbsByRole {
	
	public static final ArrayList<Double> Overall = new ArrayList<Double>(
		Arrays.asList(0.21, 0.19, 0.01, 0.07, 0.14, 0.18, 0.02, 0.18)
		)
	
	public static final ArrayList<Double> AssignedNurseAssignedRoom = new ArrayList<Double>(
			Arrays.asList(0.16, 0.18, 0.01, 0.1, 0.11, 0.17, 0.02, 0.25)
			)
	
	public static final ArrayList<Double> AssignedNurseRandom = new ArrayList<Double>(
		Arrays.asList(0.32, 0.19, 0.01 ,0.03, 0.14, 0.18, 0.01, 0.12)
		)
		
	public static final ArrayList<Double> Floater = new ArrayList<Double>(
		Arrays.asList(0.21, 0.18, 0.01, 0.06, 0.16, 0.21, 0.03, 0.14)
		)
	
	public static final ArrayList<Double> Doctor = new ArrayList<Double>(
		Arrays.asList(0.31, 0.25, 0.01, 0.01, 0.18, 0.17, 0.01, 0.06)
		)
	
	public static final ArrayList<Double> Cna = new ArrayList<Double>(
		Arrays.asList(0.29, 0.18, 0.02, 0.06, 0.17, 0.10, 0.04, 0.14)
		)
	
	public static final ArrayList<Double> Pts = new ArrayList<Double>(
		Arrays.asList(0.17, 0.22, 0.01, 0.06, 0.11, 0.27, 0.01, 0.15)
		)
		
	public static final ArrayList<Double> Rts = new ArrayList<Double>(
		Arrays.asList(0.17, 0.18, 0.01, 0.03, 0.18, 0.30, 0.01, 0.12)
		)
		
	

	public static final HashMap<String, ArrayList<Double>> PROBS = ['NURSE': VisitTypeProbsByRole.AssignedNurseAssignedRoom,
			'NURSE_RANDOM': VisitTypeProbsByRole.AssignedNurseRandom,
			'NURSE_FLOAT': VisitTypeProbsByRole.Floater,
			'DOCTOR': VisitTypeProbsByRole.Doctor,
			'CNA': VisitTypeProbsByRole.Cna,
			'PT': VisitTypeProbsByRole.Pts,
			'RT': VisitTypeProbsByRole.Rts]
}
