package agents

class Doctor extends HealthCareWorker {
	static int NEXTID=0
	int id
	
	
	Doctor(){
		super()
		id=NEXTID++
	}
	
	String toString(){
		"Doctor " + id
	}
	
}
