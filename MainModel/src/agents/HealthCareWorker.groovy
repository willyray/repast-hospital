package agents


class HealthCareWorker extends Agent {
	ArrayList<Patient> assignedPatients = new ArrayList<Patient>()
	
	HealthCareWorker(AgentType type){
		super(type)
	}
	
	public int getAssignedPatientCount() {
		return assignedPatients.size();
	}
	
	


	@Override
	public String toString() {
		return type.value + " " + agentId;
	}
	
	

}
