package agents

import actionevents.PatientMovementFrom
import actionevents.PatientMovementTo
import repast.simphony.engine.schedule.ISchedulableAction;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.List


import structure.Bed
import structure.Room

class Patient extends Agent{
	
	public static int NEXTID=0
	int id
	Bed bed
	Room room
	ISchedulableAction dischargeEvent
	
	public Patient(){
		super(AgentType.PATIENT)
		id = NEXTID++
	}
	
	public setBed(Bed b){
		if (b){
			fireActionPerformed(new PatientMovementTo(this, b.room))
		} else {
			fireActionPerformed(new PatientMovementFrom(this, bed.room))
		}
		bed = b
	}
	
	public String toString(){
		return "Patient " + id 
	}
	
	

}
