package agents

import java.awt.event.ActionEvent
import java.awt.event.ActionListener as ActionListener
import groovy.beans.ListenerList as ListenerList
import processes.Process
import repast.simphony.context.Context

class Agent extends Expando{
	 
	def static int nextAgentId = 0
	def int agentId
	def Map<String,Process> behaviors
    def List<ActionListener> listeners
	def AgentType type
	def inCommonArea = false
	def Map<String, String> attr

	Agent(AgentType type){
		behaviors = new HashMap<String,Process>()
		this.type = type
		agentId = nextAgentId++
		this.attr = new HashMap<String, String>()
	}
	
	def boolean hasAttribute(String) {
		return
	}
	
	
    public void addActionListener(ActionListener listener) {
        if ( listener == null) {
            return
        }
        if ( listeners == null) {
            listeners = []
        }
        listeners.add(listener)
    }

    public void removeActionListener(ActionListener listener) {
        if ( listener == null) {
            return
        }
        if ( listeners == null) {
            listeners = []
        }
        listeners.remove(listener)
    }

    public ActionListener[] getActionListeners() {
        Object __result = []
        if ( listeners != null) {
            __result.addAll(listeners)
        }
        return (( __result ) as ActionListener[])
    }

    public void fireActionPerformed(ActionEvent param0) {
        if ( listeners != null) {
            ArrayList<ActionListener> __list = new ArrayList<ActionListener>(listeners)
            for (def listener : __list ) {
                listener.actionPerformed(param0)
            }
        }
    }

	@Override
	public String toString() {
		return "Agent [agentId=" + agentId + ", type=" + type + "]";
	}
	
	
}
