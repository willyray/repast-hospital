package agents;

public enum AgentType {
	PATIENT("PATIENT"), HEALTHCAREWORKER("HCW"), NURSE("NURSE"), NURSE_FLOAT("NURSE_FLOAT"), CNA("CNA")
	, DOCTOR("DOCTOR"), PT("PT"), RT("RT"), AGENT("AGENT"), ROOM("ROOM"), PROCESS("PROCESS");
	
	private String value;
	
	private AgentType(String v) {
		this.value = v;
	}
}
