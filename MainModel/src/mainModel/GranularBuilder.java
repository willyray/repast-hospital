package mainModel;
//test
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import actionevents.ColonizedEvent;
import actionevents.ContaminationPulseEvent;
import actionevents.DiseaseProgression;
import actionevents.ImportationEvent;
import actionevents.PatientAdmitted;
import actionevents.PatientDischarged;
import actionevents.RoomLeave;
import actionevents.RoomVisit;
import actionevents.TouchSurfaceAction;
import actionevents.WashHandsEvent;
import agents.Agent;
import agents.AgentType;
import agents.HealthCareWorker;
import agents.Nurse;
import agents.Patient;
import behavior.AssignedPatientVisitBehavior;
import behavior.CommonAreaBehaviorSet;
import behavior.RoomVisitDuration;
import behavior.StochasticRoomVisit;
import cern.jet.random.Gamma;
import datacollectors.RuntimeData;
import granular.behavior.GranularContactMan;
import granular.behavior.GranularVisit;
import granular.behavior.HandHygieneManager;
import granular.behavior.VisitType;
import granular.disease.Disease;
import granular.disease.DiseaseManager;
import metacomponents.GranularMetaConfiguration;
import metacomponents.RoomVisitComponent;
import processes.roomcleaning.CleaningShiftStart;
import repast.simphony.context.Context;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.graph.Network;
import repast.simphony.util.collections.IndexedIterable;
import simple_adt.Admission;
import simple_adt.Discharge;
import structure.Admittable;
import structure.Bed;
import structure.Dischargeable;
import structure.Facility;
import structure.Room;
import structure.Surface;
import structure.Ward;
import util.OutputFile;
import util.TimeUtils;
import processes.ContactNetworkManager;
import processes.DoctorAssignmentManager;
import processes.Process;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.IteratorUtils;

public class GranularBuilder implements ContextBuilder<Object>, Admittable, Dischargeable, ActionListener{
	
	private Ward ward;
	private ArrayList<Room> rooms;
	private Admission admission;
	private Discharge discharger;
	private CleaningShiftStart cleaning;
	private ISchedule schedule;
	private ActionController actionController;
	private RuntimeData data;
	private OutputFile file;
	//private ArrayList<Object> commonArea;
	private ArrayList<Patient> patients;
	private ContactNetworkManager contactNet;
	private DoctorAssignmentManager doctorAssignmentManager;
	public Network<Object> assigned,commonArea,doctorAssignments;
	public ArrayList<HealthCareWorker> nurses,floaters,cnas,doctors,pts,rts;
	private HashMap<String, Gamma> visitDistributions;
	private HashMap<VisitType, Gamma> visitDurations;
	private HandHygieneManager handHygieneMan;

	private DiseaseManager diseaseMan;
	
	
	private FileWriter eventWriter;

	
	//Params: 
	private int roomCount = 20; 
	private double nursePerRoom = 0.5;
	private int doctorCount = 6;
	private int floatingNurseCount = 2;
	private int cnaCount = 2;
	private int rtCount = 2;
	private int ptCount = 2;
	private final double tenmin = 10*TimeUtils.MINUTE;
	
	
	@ScheduledMethod(start=1.0,interval=1.0)
	public void daily(){
		cleaning.start();
		event("OCCUPANCY", Integer.toString(patients.size()));
		
		if (TimeUtils.getSchedule().getTickCount() >= 60) {
			
			double one = 1.0;
		}
		
	}
	
	
	
	@ScheduledMethod(start=90, interval=1)
	public void endDate(){
		RunEnvironment.getInstance().endRun();
	}
	
	private Gamma getGamma(double shape, double scale) {
		double alpha = shape;
		double lambda = 1.0/scale;
		return RandomHelper.createGamma(alpha, lambda);
		
	}
	
	
	@Override
	public Context build(Context<Object> context) {
		
		
		schedule = repast.simphony.engine.environment.RunEnvironment.getInstance().getCurrentSchedule();
		
		schedule.schedule(this);
		
		ward = new Ward();
		ward.setId("MainModel");
		rooms = new ArrayList<Room>();
		createRooms(roomCount);
		nurses = createNurses(ward,nursePerRoom);
		floaters = createfloaters(ward, floatingNurseCount);
		cnas = createCnas(ward, cnaCount);
		doctors = createDoctors(ward, doctorCount);
		pts = createPts(ward, ptCount);
		rts = createRts(ward, rtCount);
		
		GranularMetaConfiguration.configure();
		
		//commonArea = new ArrayList<Object>();
		NetworkBuilder<Object> assignedBuilder = new NetworkBuilder<Object>("assigned", ward, false);
		assigned = assignedBuilder.buildNetwork();
		NetworkBuilder<Object> commonBuider = new NetworkBuilder<Object>("commonArea", ward, false);
		commonArea = commonBuider.buildNetwork();
		NetworkBuilder<Object> doctorBuilder = new NetworkBuilder<Object>("doctors", ward, false);
		doctorAssignments = doctorBuilder.buildNetwork();
		
		visitDistributions = new HashMap<String, Gamma>();
		visitDistributions.put("NURSE", getGamma(0.62, 51.0));
		visitDistributions.put("NURSE_RANDOM", getGamma(0.57, 182.0));
		visitDistributions.put("NURSE_FLOAT", getGamma(0.62, 110));
		visitDistributions.put("CNA", getGamma(0.54, 137));
		visitDistributions.put("DOCTOR", getGamma(0.54, 168));
		visitDistributions.put("PT", getGamma(0.48, 167));
		visitDistributions.put("RT", getGamma(0.60, 178));
		
		
		visitDurations = new HashMap<VisitType, Gamma>();
		visitDurations.put(VisitType.NONEAR_FEW_SHORT, getGamma(11.1, 0.31));
		visitDurations.put(VisitType.NONEAR_FEW_LONG, getGamma(3.9, 3.10));
		visitDurations.put(VisitType.NONEAR_LOTS_SHORT, getGamma(14.1, 0.29));
		visitDurations.put(VisitType.NONEAR_LOTS_LONG, getGamma(3.1, 5.04));
		visitDurations.put(VisitType.NEAR_FEW_SHORT, getGamma(11.1, 0.32));
		visitDurations.put(VisitType.NEAR_FEW_LONG, getGamma(3.4, 3.98));
		visitDurations.put(VisitType.NEAR_LOTS_SHORT, getGamma(16.0, 0.24));
		visitDurations.put(VisitType.NEAR_LOTS_LONG, getGamma(3.0,7.0));
		
		
		
		
		diseaseMan = new DiseaseManager(this);
		diseaseMan.addActionListener(this);
		
		handHygieneMan = new HandHygieneManager();
		
		
		
		patients = new ArrayList<Patient>();
		admission = new Admission(.25, this);
		admission.start();
		
		discharger = new Discharge(5.5, this);
		
		
		ContactNetworkManager nurseContacts = new GranularContactMan(0.5, nurses, ward, this, assigned);
		nurseContacts.start();
		doctorAssignmentManager = new DoctorAssignmentManager(doctors, ward, this, doctorAssignments);
		
		for (Object n : nurses) {
			if (n instanceof HealthCareWorker && ((HealthCareWorker) n).getType() == AgentType.NURSE) {
				HealthCareWorker nurse = (HealthCareWorker)n;
				nurse.setProperty("surface", new Surface(nurse.toString(),23));
				//AssignedPatientVisitBehavior avisit = new AssignedPatientVisitBehavior(1.0/24, nurse, this, assigned);
				//nurse.getBehaviors().put("AVISIT", avisit);
				//avisit.start();
				GranularVisit gv_nurse_random = new GranularVisit(visitDistributions.get("NURSE_RANDOM"), nurse, assigned, false, visitDurations, this);
				GranularVisit gv_nurse = new GranularVisit(visitDistributions.get("NURSE"), nurse, assigned, true, visitDurations, this);
				nurse.getBehaviors().put("assiged_visit", gv_nurse);
				nurse.getBehaviors().put("random_visit", gv_nurse_random);
				gv_nurse.start();
				gv_nurse_random.start();
				gv_nurse.addActionListener(this);
				gv_nurse_random.addActionListener(this);
				
			}	
		}
		
		for (HealthCareWorker floater : floaters) {
			if (floater instanceof HealthCareWorker) {
				HealthCareWorker f = (HealthCareWorker) floater;
				floater.setProperty("surface", new Surface(floater.toString(),23));
				if (( (HealthCareWorker) floater).getType() == AgentType.NURSE_FLOAT){
					GranularVisit gv_float = new GranularVisit(visitDistributions.get("NURSE_FLOAT"), f, assigned, false, visitDurations, this);
					f.getBehaviors().put("random_visit", gv_float);
					gv_float.start();
					gv_float.addActionListener(this);
				}
			}
		}
		
		for (HealthCareWorker cna : cnas) {
			if (cna instanceof HealthCareWorker) {
				HealthCareWorker c = (HealthCareWorker) cna;
				cna.setProperty("surface", new Surface(cna.toString(),23));
				if (c.getType() == AgentType.NURSE_FLOAT){
					GranularVisit gv_cna = new GranularVisit(visitDistributions.get("CNA"), c, assigned, false, visitDurations, this);
					c.getBehaviors().put("random_visit", gv_cna);
					gv_cna.start();
					gv_cna.addActionListener(this);
				}
			}
		}		
		
		for (HealthCareWorker doctor : doctors) {
			if (doctor instanceof HealthCareWorker) {
				HealthCareWorker f = (HealthCareWorker) doctor;
				doctor.setProperty("surface", new Surface(doctor.toString(),23));
				if (( (HealthCareWorker) doctor).getType() == AgentType.DOCTOR){
					GranularVisit gv_doctor = new GranularVisit(visitDistributions.get("DOCTOR"), doctor, doctorAssignments, true, visitDurations, this);
					doctor.getBehaviors().put("assigned", gv_doctor);
					gv_doctor.start();
					gv_doctor.addActionListener(this);
				}
			}
		}
		
		for (HealthCareWorker pt : pts) {
			if (pt instanceof HealthCareWorker) {
				HealthCareWorker p = (HealthCareWorker) pt;
				pt.setProperty("surface", new Surface(pt.toString(),23));
				if (( (HealthCareWorker) pt).getType() == AgentType.PT){
					GranularVisit gv_pt = new GranularVisit(visitDistributions.get("PT"), p, assigned, false, visitDurations, this);
					p.getBehaviors().put("random_visit", gv_pt);
					gv_pt.start();
					gv_pt.addActionListener(this);
				}
			}
		}
		
		for (HealthCareWorker rt : rts) {
			if (rt instanceof HealthCareWorker) {
				HealthCareWorker r = (HealthCareWorker) rt;
				rt.setProperty("surface", new Surface(rt.toString(),23));
				if (( (HealthCareWorker) rt).getType() == AgentType.RT){
					GranularVisit gv_rt = new GranularVisit(visitDistributions.get("RT"), r, assigned, false, visitDurations, this);
					r.getBehaviors().put("random_visit", gv_rt);
					gv_rt.start();
					gv_rt.addActionListener(this);
				}
			}
		}
		

		
		
		//commonArea = new ArrayList<Object>();
		cleaning = new CleaningShiftStart(1, .5, ward.getRooms(), this);
		cleaning.addActionListener(this);
		cleaning.addActionListener(diseaseMan);
		
		
		
		return ward;
	}
	
	public void setContactNetworks() {
		
	}
	

	//Process implementations
	@Override
	public void admit() {
		if (ward.getVacancy()>0){
			
			ArrayList<Room> emptyRooms = new ArrayList<Room>();
			for (Room r : ward.getRooms()){
				if (r.getOccupancy()==0){
					emptyRooms.add(r);
				}
			}
			
			if (emptyRooms.size()>0){
				Room targetRoom = emptyRooms.get(new Random().nextInt(emptyRooms.size()));
				
				Patient p = new Patient();
				p.setProperty("surface", new Surface(p.toString(),300));
				p.addActionListener(this);
				p.addActionListener(doctorAssignmentManager);
				p.addActionListener(diseaseMan);
				p.fireActionPerformed(new PatientAdmitted(p));
				targetRoom.getBeds().get(0).setPatient(p);
				p.setBed(targetRoom.getBeds().get(0));
				p.setRoom(targetRoom);
				
				
				ward.add(p);
				patients.add(p);
				//System.out.println("count: " + patients.size());
				event("ADMISSION", "add patient " + p.getId() + " to Room " + targetRoom.getRoomNumber());
				discharger.newPatient(p);
			}
			else{
				event("ADMISSION", "tried to add patient - couldn't find room");
			}
			
		} else {
			//no action, hospital is full
			event("ADMISSION", "Hospital is full");
			}
	}
	
	@Override
	public void discharge(Patient p) {
		p.getBed().setPatient(null);
		p.setBed(null);
		p.fireActionPerformed(new PatientDischarged(p));
		event("DISCHARGE", "Discharged patient " + p.toString());
		for (Object hcw : assigned.getSuccessors(p)) {
			if (hcw instanceof HealthCareWorker) {
				HealthCareWorker h = (HealthCareWorker)hcw;
				h.getAssignedPatients().remove(p);
			}
			
		}
		if (p.getProperty("disease") != null) {
			((Disease) p.getProperty("disease")).stop();
		}
		ward.remove(p);
		patients.remove(p);
	}
	
	
	
	private void createRooms(int count){
		event("INIT", "create " + count + " rooms");
		for (int i=0; i<count; i++){
			Room r = new Room(i+"",true, false, false, false, false, ward);
			r.setCapacity(1);
			Bed b = new Bed(r);
			r.addBed(b);
			b.setRoom(r);
			Surface touchscreen = new Surface("touchscreen", 301);
			Surface table = new Surface("table", 132);
			r.getFarZone().add(touchscreen);
			r.getFarZone().add(table);
			Surface cart = new Surface("cart", 301);
			Surface keyboard = new Surface("keyboard", 301);
			r.getNearZone().add(cart);
			r.getNearZone().add(keyboard);
			
			
			
			rooms.add(r);
			ward.addRoom(r);
		}
			
	}
	
	private ArrayList<HealthCareWorker> createNurses(Ward ward, double ratio){
		int bedCount = ward.getRooms().size();
		int nurseCount = (int) java.lang.Math.ceil(bedCount*ratio);
		event("INIT", "Create " + nurseCount + " nurses");
		ArrayList<HealthCareWorker> nurses = new ArrayList<HealthCareWorker>();
		for (int i = 0; i<nurseCount; i++){
			HealthCareWorker n = new HealthCareWorker(AgentType.NURSE);
			n.setInCommonArea(true);
			ward.add(n);
			ward.getNurses().add(n);
			nurses.add(n);
			CommonAreaBehaviorSet c_mix = new CommonAreaBehaviorSet(.05, n, this, commonArea);	
			n.getBehaviors().put("COMMONMIX", c_mix);
			c_mix.start();
			n.setProperty("surface", new Surface(n.toString()));
			
		}		
		return nurses;
		
	}
	
	private ArrayList<HealthCareWorker> createfloaters(Ward ward, int floatingNurseCount){
		ArrayList<HealthCareWorker> floaters = new ArrayList<HealthCareWorker>();
		for (int i = 0; i<floatingNurseCount; i++) {
			HealthCareWorker n = new HealthCareWorker(AgentType.NURSE_FLOAT);
			n.setInCommonArea(true);
			ward.add(n);
			
			floaters.add(n);
			CommonAreaBehaviorSet c_mix = new CommonAreaBehaviorSet(.05, n, this, commonArea);	
			n.getBehaviors().put("COMMONMIX", c_mix);
			c_mix.start();
			n.setProperty("surface", new Surface(n.toString()));

		}
		return floaters;
	}
		
	private ArrayList<HealthCareWorker> createCnas(Ward ward, int cnaCount){
		ArrayList<HealthCareWorker> cnas = new ArrayList<HealthCareWorker>();
		for (int i = 0; i<cnaCount; i++) {
			HealthCareWorker n = new HealthCareWorker(AgentType.CNA);
			n.setInCommonArea(true);
			ward.add(n);
			
			cnas.add(n);
			CommonAreaBehaviorSet c_mix = new CommonAreaBehaviorSet(.05, n, this, commonArea);	
			n.getBehaviors().put("COMMONMIX", c_mix);
			c_mix.start();
			n.setProperty("surface", new Surface(n.toString()));

		}
		return cnas;
		
	}
	
	private ArrayList<HealthCareWorker> createDoctors(Ward ward, int doctorCount){
		ArrayList<HealthCareWorker> docs = new ArrayList<HealthCareWorker>();
		for (int i = 0; i<doctorCount; i++) {
			HealthCareWorker n = new HealthCareWorker(AgentType.DOCTOR);
			n.setInCommonArea(true);
			ward.add(n);
			
			docs.add(n);
			CommonAreaBehaviorSet c_mix = new CommonAreaBehaviorSet(.05, n, this, commonArea);	
			n.getBehaviors().put("COMMONMIX", c_mix);
			c_mix.start();
			n.setProperty("surface", new Surface(n.toString()));

		}
		return docs;
	}
	
	private ArrayList<HealthCareWorker> createPts(Ward ward, int ptCount){
		ArrayList<HealthCareWorker> pts = new ArrayList<HealthCareWorker>();
		for (int i = 0; i<ptCount; i++) {
			HealthCareWorker n = new HealthCareWorker(AgentType.PT);
			n.setInCommonArea(true);
			ward.add(n);
			
			pts.add(n);
			CommonAreaBehaviorSet c_mix = new CommonAreaBehaviorSet(.05, n, this, commonArea);	
			n.getBehaviors().put("COMMONMIX", c_mix);
			c_mix.start();
			n.setProperty("surface", new Surface(n.toString()));

		}
		return pts;
		
	}
	
	private ArrayList<HealthCareWorker> createRts(Ward ward, int rtCount){
		ArrayList<HealthCareWorker> rts = new ArrayList<HealthCareWorker>();
		for (int i = 0; i<rtCount; i++) {
			HealthCareWorker n = new HealthCareWorker(AgentType.RT);
			n.setInCommonArea(true);
			ward.add(n);
			
			rts.add(n);
			CommonAreaBehaviorSet c_mix = new CommonAreaBehaviorSet(.05, n, this, commonArea);	
			n.getBehaviors().put("COMMONMIX", c_mix);
			c_mix.start();
			n.setProperty("surface", new Surface(n.toString()));

		}
		return rts;
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e instanceof ImportationEvent) {
			ImportationEvent ie = (ImportationEvent)e;
			event("IMPORTATION", "by " + ie.getPatient().toString());
		}
		
		if (e instanceof ContaminationPulseEvent) {
			ContaminationPulseEvent ce = (ContaminationPulseEvent)e;
			event("CONTAMINATION_PULSE", ce.patient.toString());
			if (TimeUtils.getSchedule().getTickCount() >= 60) {
				ce.patient.hashCode();
			}
		}
		
		if (e instanceof DiseaseProgression) {
			DiseaseProgression de = (DiseaseProgression)e;
			event("PROGRESSION", de.patient.toString());
		}
		
		if (e instanceof TouchSurfaceAction) {
			
		}
		
		if (e instanceof ColonizedEvent) {
			ColonizedEvent ce = (ColonizedEvent)e;
			event("COLONIZATION", ce.patient.toString() + " (load: " + ce.load + ")");
		}
		
		if (e instanceof RoomVisit) {
			
			
		}
		
		if (e instanceof WashHandsEvent) {
			WashHandsEvent whe = (WashHandsEvent)e;
			
			
			diseaseMan.actionPerformed(e);
			event("HANDWASH", whe.getHcw().toString() + ", " + whe.getHhType().toString());
			
			
		}
	}
	
	
	
	public void event(String type, String line) {
		if (schedule.getTickCount() < 60) {
			return;
		}
		if (eventWriter == null) {
			try {
				eventWriter = new FileWriter("events.csv");
			} catch (IOException e) {
				System.out.println("Nope");
				e.printStackTrace();
			}
		}
		
		try {
			
			eventWriter.write(schedule.getTickCount() + "," + type + ","+ line + "\n");
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	


	
	


	

	




}
