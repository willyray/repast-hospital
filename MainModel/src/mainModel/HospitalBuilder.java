package mainModel;
//test
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import actionevents.PatientAdmitted;
import actionevents.PatientDischarged;
import actionevents.RoomLeave;
import actionevents.RoomVisit;
import agents.Nurse;
import agents.Patient;
import behavior.RoomVisitDuration;
import behavior.StochasticRoomVisit;
import datacollectors.RuntimeData;
import metacomponents.RoomVisitComponent;
import processes.roomcleaning.CleaningShiftStart;
import repast.simphony.context.Context;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import simple_adt.Admission;
import simple_adt.Discharge;
import structure.Admittable;
import structure.Bed;
import structure.Dischargeable;
import structure.Facility;
import structure.Room;
import structure.Ward;
import util.OutputFile;
import util.TimeUtils;
import processes.Process;

public class HospitalBuilder implements ContextBuilder<Object>, Admittable, Dischargeable, ActionListener{
	private Facility hospital;
	private Ward sicu,micu,acute,medsurg,telem;
	private ArrayList<Room> rooms;
	private Admission admission;
	private Discharge discharger;
	private CleaningShiftStart cleaning;
	private ISchedule schedule;
	private ActionController actionController;
	private RuntimeData data;
	private OutputFile file;
	
	
	@ScheduledMethod(start=1,interval=1)
	public void daily(){
		cleaning.start();
	}
	
	@ScheduledMethod(start=90, interval=1)
	public void endDate(){
		RunEnvironment.getInstance().endRun();
	}
	
	
	@Override
	public Context build(Context<Object> context) {
		String outputHead = "tick,eventid,source,data";
		file = new OutputFile(outputHead, "outputs.txt");
		file.createFile();
		//Add specific methods to metaClasses
		RoomVisitComponent.configureRoomVisit();
		
		schedule = repast.simphony.engine.environment.RunEnvironment.getInstance().getCurrentSchedule();
		hospital = new Facility();
		rooms = new ArrayList<Room>();
		this.actionController = new ActionController();
		buildWards();
		buildRooms();
		buildBeds();
		calculateCapacity();
		buildNurses();
		data = new RuntimeData(schedule);
		schedule.schedule(this);
		
		
		
		admission = new Admission(.1, this);
		admission.start();
		
		discharger = new Discharge(5.5, this);
		
		//cleaning = new CleaningShiftStart(TimeUtils.HOUR*2, TimeUtils.HOUR*22, rooms);
		//cleaning.start();
		
		
		
		
		
		
		
		
				
		
		
		
		
		
		hospital.setId("MainModel");
		return hospital;
		
		
		
	}
	
	

	//Process implementations
	@Override
	public void admit() {
		if (hospital.getVacancy()>0){
			
			Bed targetBed = (Bed)hospital.getEmptyBeds().get(new Random().nextInt(hospital.getEmptyBeds().size()));
			Patient p = new Patient();
			p.addActionListener(this);
			p.fireActionPerformed(new PatientAdmitted(p));
			targetBed.setPatient(p);
			p.setBed(targetBed);
			
			hospital.add(p);
			
			
		} else {
			//no action, hospital is full
			}
		
	}
	
	@Override
	public void discharge(Patient p) {
		p.getBed().setPatient(null);
		p.setBed(null);
		p.fireActionPerformed(new PatientDischarged(p));
		hospital.remove(p);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String outputLine = schedule.getTickCount() + "," +e.getID() + "," + e.getSource().toString() + "," + e.getActionCommand();
 		file.writeLine(outputLine);
		//System.out.println(schedule.getTickCount() +   "," + e.getSource().toString() + "," + e.getActionCommand());
		if (e instanceof PatientAdmitted){
			discharger.newPatient(((PatientAdmitted) e).getPatient());
		}
		if (e instanceof PatientDischarged){
			//do patient discharge things
		}	
		if (e instanceof RoomVisit){
			RoomVisit rve = (RoomVisit)e;
			rve.getHcw().getBehaviors().get("ROOMVISIT").stop();
			RoomVisitDuration visitLength = new RoomVisitDuration(rve.getHcw(), rve.getRoom(), 15*TimeUtils.MINUTE);
			visitLength.start();
			rve.getHcw().getBehaviors().put("ROOMEXIT", visitLength);
			
			//action controller is groovy; has access to methods on the metaclass
			actionController.healthCareWorkerVisitActions(rve);
			data.setVisitCount(data.getVisitCount()+1);
		}
		
		if (e instanceof RoomLeave){
			RoomLeave rle = (RoomLeave)e;
			rle.getHealthCareWorker().getBehaviors().remove("ROOMEXIT");
			rle.getHealthCareWorker().getBehaviors().get("ROOMVISIT").start();
		}
	}


	
	//Physical Init
	private void calculateCapacity() {
		for (Room r: sicu.getRooms()){
			sicu.setCapacity(sicu.getCapacity()+r.getCapacity());
		}
		for (Room r: micu.getRooms()){
			micu.setCapacity(micu.getCapacity()+r.getCapacity());
		}
		for (Room r: medsurg.getRooms()){
			medsurg.setCapacity(medsurg.getCapacity()+r.getCapacity());
		}
		for (Room r: acute.getRooms()){
			acute.setCapacity(acute.getCapacity()+r.getCapacity());
		}
		hospital.setCapacity(micu.getCapacity()+sicu.getCapacity()+medsurg.getCapacity()+acute.getCapacity());
		System.out.println("capacity = " + hospital.getCapacity());
	}


	private void buildBeds() {
		for (Room r : rooms){
			int bedcount=1;
			if (!r.isSinglePatient()){
				bedcount = 2;
			}
			
			for (int i=0; i<bedcount; i++){
				Bed b = new Bed(r);
				r.addBed(b);
				hospital.getBeds().add(b);
			}
		}
		
	}


	private void buildWards() {
		sicu = new Ward(hospital);
		micu = new Ward(hospital);
		acute = new Ward(hospital);
		medsurg = new Ward(hospital);
		//telem = new Ward();
		
		hospital.addWard(sicu);
		hospital.addWard(micu);
		hospital.addWard(acute);
		hospital.addWard(medsurg);
		//hospital.addWard(telem);
	}
	
	private void buildNurses() {
		//micu nurses
		for (int i=0;i<2;i++){
			Nurse n = new Nurse();
			hospital.add(n);
			micu.getNurses().add(n);
			n.addActionListener(this);
			n.getBehaviors().put("ROOMVISIT", new StochasticRoomVisit(n, micu, TimeUtils.HOUR*4, TimeUtils.MINUTE*12));
			n.getBehaviors().get("ROOMVISIT").start();
		}
		
		//sicu nurses
		for (int i=0;i<2;i++){
			Nurse n = new Nurse();
			hospital.add(n);
			sicu.getNurses().add(n);
			n.addActionListener(this);
			n.getBehaviors().put("ROOMVISIT", new StochasticRoomVisit(n, sicu, TimeUtils.HOUR*4, TimeUtils.MINUTE*12));
			n.getBehaviors().get("ROOMVISIT").start();
		}
		
		//acute nurses
		for (int i=0;i<4;i++){
			Nurse n = new Nurse();
			hospital.add(n);
			acute.getNurses().add(n);
			n.addActionListener(this);
			n.getBehaviors().put("ROOMVISIT", new StochasticRoomVisit(n, acute, TimeUtils.HOUR*4, TimeUtils.MINUTE*12));
			n.getBehaviors().get("ROOMVISIT").start();
		}
		//medsurg nurses
		for (int i=0;i<4;i++){
			Nurse n = new Nurse();
			hospital.add(n);
			medsurg.getNurses().add(n);
			n.addActionListener(this);
			n.getBehaviors().put("ROOMVISIT", new StochasticRoomVisit(n, medsurg, TimeUtils.HOUR*4, TimeUtils.MINUTE*12));
			n.getBehaviors().get("ROOMVISIT").start();
		}
		
		
		
	}
	private void buildRooms() {
		//Create micu rooms
		Room m1 = new Room();
		m1.setRoomNumber("2A21");
		m1.setSinglePatient(true);
		m1.setBathroom(false);
		m1.setNegativePressure(false);
		m1.setWard(micu);
		micu.addRoom(m1);
		
		Room m2 = new Room();
		m2.setRoomNumber("2A23");
		m2.setSinglePatient(true);
		m2.setBathroom(false);
		m2.setNegativePressure(false);
		m2.setWard(micu);
		micu.addRoom(m2);
		
		Room m3 = new Room();
		m3.setRoomNumber("2A25");
		m3.setSinglePatient(true);
		m3.setBathroom(false);
		m3.setNegativePressure(false);
		m3.setWard(micu);
		micu.addRoom(m3);
		
		Room m4 = new Room();
		m4.setRoomNumber("2A27");
		m4.setSinglePatient(true);
		m4.setBathroom(false);
		m4.setNegativePressure(false);
		m4.setWard(micu);
		micu.addRoom(m4);
		
		Room m5 = new Room();
		m5.setRoomNumber("2A29");
		m5.setSinglePatient(true);
		m5.setBathroom(false);
		m5.setNegativePressure(false);
		m5.setWard(micu);
		micu.addRoom(m5);
		
		Room m6 = new Room();
		m6.setRoomNumber("2A231");
		m6.setSinglePatient(true);
		m6.setBathroom(false);
		m6.setNegativePressure(false);
		m6.setWard(micu);
		micu.addRoom(m6);
		
		Room m7 = new Room();
		m7.setRoomNumber("2A33A");
		m7.setSinglePatient(true);
		m7.setBathroom(false);
		m7.setNegativePressure(true);
		m7.setIsolation(true);
		m7.setWard(micu);
		micu.addRoom(m7);
		
		Room m8 = new Room();
		m8.setRoomNumber("2A35A");
		m8.setSinglePatient(true);
		m8.setBathroom(false);
		m8.setNegativePressure(true);
		m8.setIsolation(true);
		m8.setWard(micu);
		micu.addRoom(m8);
		
		rooms.addAll(micu.getRooms());
		
		
		//create sicu rooms
		Room s1 = new Room();
		s1.setRoomNumber("3D11");
		s1.setSinglePatient(true);
		s1.setBathroom(false);
		s1.setNegativePressure(false);
		s1.setWard(sicu);
		sicu.addRoom(s1);
		
		Room s2 = new Room();
		s2.setRoomNumber("3D13");
		s2.setSinglePatient(true);
		s2.setBathroom(false);
		s2.setNegativePressure(false);
		s2.setWard(sicu);
		sicu.addRoom(s2);
		
		Room s3 = new Room();
		s3.setRoomNumber("3D15");
		s3.setSinglePatient(true);
		s3.setBathroom(false);
		s3.setNegativePressure(false);
		s3.setWard(sicu);
		sicu.addRoom(s3);
		
		Room s4 = new Room();
		s4.setRoomNumber("3D17");
		s4.setSinglePatient(true);
		s4.setBathroom(true);
		s4.setSharedBathroom(true);
		s4.setNegativePressure(false);
		s4.setWard(sicu);
		sicu.addRoom(s4);
		
		Room s5 = new Room();
		s5.setRoomNumber("3D19");
		s5.setSinglePatient(true);
		s5.setBathroom(true);
		s5.setSharedBathroom(true);
		s5.setNegativePressure(false);
		s5.setWard(sicu);
		sicu.addRoom(s5);
		
		Room s6 = new Room();
		s6.setRoomNumber("3D21");
		s6.setSinglePatient(true);
		s6.setBathroom(false);
		s6.setWard(sicu);
		sicu.addRoom(s6);
		
		Room s7 = new Room();
		s7.setRoomNumber("3D23A");
		s7.setSinglePatient(true);
		s7.setBathroom(false);
		s7.setNegativePressure(true);
		s7.setIsolation(true);
		s7.setWard(sicu);
		sicu.addRoom(s7);
		
		Room s8 = new Room();
		s8.setRoomNumber("3D27");
		s8.setSinglePatient(true);
		s8.setBathroom(false);
		s8.setWard(sicu);
		sicu.addRoom(s8);
		
		Room s9 = new Room("3D29",true,false,false,false,false,sicu);
		sicu.addRoom(s9);
		
		rooms.addAll(sicu.getRooms());
		
		//build acute rooms
		acute.addRoom(new Room("2A01",true,true,false,false,false,acute));
		acute.addRoom(new Room("2A02",false,true,true,false,false,acute));
		acute.addRoom(new Room("2A04",false,true,true,false,false,acute));
		acute.addRoom(new Room("2A06",false,true,true,false,false,acute));
		acute.addRoom(new Room("2B11",true,true,true,false,false,acute));
		acute.addRoom(new Room("2B13",false,true,true,false,false,acute));
		acute.addRoom(new Room("2B14",false,true,true,false,false,acute));
		acute.addRoom(new Room("2B17",true,true,false,false,false,acute));
		acute.addRoom(new Room("2B18",true,true,false,false,false,acute));
		acute.addRoom(new Room("2B19",true,true,false,false,false,acute));
		acute.addRoom(new Room("2B20",true,true,false,false,false,acute));
		acute.addRoom(new Room("2B24",true,true,false,false,false,acute));
		acute.addRoom(new Room("2B26",true,true,false,false,false,acute));
		acute.addRoom(new Room("2B28",true,true,false,false,false,acute));
		acute.addRoom(new Room("2C01",true,true,false,false,false,acute));
		acute.addRoom(new Room("2C02",false,true,true,false,false,acute));
		acute.addRoom(new Room("2C03",true,true,false,false,false,acute));
		acute.addRoom(new Room("2C04",false,true,true,false,false,acute));
		acute.addRoom(new Room("2C05",true,true,false,true,true,acute));
		acute.addRoom(new Room("2C06",true,true,false,false,false,acute));
		acute.addRoom(new Room("2C14",true,true,false,true,true,acute));
		acute.addRoom(new Room("2C16",true,true,false,false,false,acute));
		acute.addRoom(new Room("2C19",false,true,true,false,false,acute));
		rooms.addAll(acute.getRooms());
		
		//build medsurg rooms
		medsurg.addRoom(new Room("3A01",true,true,false,false,false,medsurg));
		medsurg.addRoom(new Room("3A02",false,true,false,false,false,medsurg));
		medsurg.addRoom(new Room("3A03",false,true,true,false,false,medsurg));
		medsurg.addRoom(new Room("3A04",false,true,true,false,false,medsurg));
		medsurg.addRoom(new Room("3A05",false,true,true,false,false,medsurg));
		medsurg.addRoom(new Room("3A06",false,true,true,false,false,medsurg));
		medsurg.addRoom(new Room("3A07",false,true,true,false,false,medsurg));
		medsurg.addRoom(new Room("3A08",false,true,true,false,false,medsurg));
		medsurg.addRoom(new Room("3A09",false,true,true,false,false,medsurg));
		medsurg.addRoom(new Room("3A17",false,true,false,false,false,medsurg));
		medsurg.addRoom(new Room("3A19",true,true,false,true,true,medsurg));
		medsurg.addRoom(new Room("3A21",true,true,false,true,true,medsurg));
		medsurg.addRoom(new Room("3A24",false,true,true,false,false,medsurg));
		medsurg.addRoom(new Room("3A26",false,true,true,false,false,medsurg));
		medsurg.addRoom(new Room("3A29",true,true,false,false,false,medsurg));
		medsurg.addRoom(new Room("3A31",false,true,true,false,false,medsurg));
		medsurg.addRoom(new Room("3A35",true,true,false,false,false,medsurg));
		rooms.addAll(medsurg.getRooms());
		
		for (Room r: rooms){
			r.addActionListener(this);
		}
	}

	

	




}
