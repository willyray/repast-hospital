	package util

import java.awt.event.ActionEvent;

import groovy.transform.WithWriteLock;

class OutputFile {
	String header
	String fileName
	File file
	
	public OutputFile(String h, String f) {
		header=h
		fileName=f
		file = new File(fileName)
		
		createFile()
	}
	
	def createFile(){
		file.withWriter('UTF-8') {
			it.writeLine header
		} 
	}
	
	def writeEvent(ActionEvent e){
		
	}
	
	def writeLine(String line){
		file << line + '\n'
	}

}
