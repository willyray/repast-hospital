package processes.roomcleaning

import actionevents.RoomCleaningEvent
import mainModel.GranularBuilder
import processes.Process
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import structure.Room;

class CleaningShiftStart extends Process {
	double shiftLength
	double Started
	double shiftStartTime
	boolean firstRoom = true
	List<Room> allRooms
	List<Room> rooms 
	GranularBuilder main
	
	public CleaningShiftStart( double shiftLength, double shiftStartTime, List<Room> rooms, GranularBuilder main) {
		super(shiftLength/rooms.size());
		started = schedule.getTickCount();
		shiftLength = shiftLength
		shiftStartTime = shiftStartTime
		allRooms = rooms
		this.main = main
		
		
	}

	@Override
	public Object start() {
		rooms = allRooms.collect()
		Collections.shuffle(this.rooms)
		firstRoom =true;
		scheduleRoom()
		
		
		
		
		
		
		
		
		return null;
	}
	
	def scheduleRoom(){
		Room r = rooms.remove(0)
		double offsetTime
		if (firstRoom){
			firstRoom=false
			offsetTime = shiftStartTime
		} else {
			offsetTime = 0
		}
		
		def currTime = schedule.getTickCount()
		def eventTime = currTime + distro.sample() + offsetTime
		ScheduleParameters params = ScheduleParameters.createOneTime(eventTime)
		schedule.schedule(params, this, "cleanRoom", r)
		//println("next room cleaning at " + eventTime + ", rooms to clean: " + rooms.size())

		
	}
	
	def cleanRoom(Room r){
		r.clean()
		fireActionPerformed(new RoomCleaningEvent(r))
		main.event("CLEAN", r.toString())
		
		if (rooms.size()){
			scheduleRoom()
		}
	}
	
	

	
	public Object stop() {
		return null;
	}
	
	

}
