package processes;

import java.util.ArrayList;

import agents.HealthCareWorker;
import agents.Patient;
import mainModel.GranularBuilder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.schedule.ISchedulableAction;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.space.graph.Network;
import structure.Ward;

public class ContactNetworkManager extends Process{
	private ArrayList<HealthCareWorker> hcws;
	private Ward ward;
	private double shiftRepeat = 0.5;
	private GranularBuilder main;
	private Network<Object> assigned;
	private boolean stopped;
	private ISchedule schedule;
	
	public ContactNetworkManager(double intra, ArrayList<HealthCareWorker> hcws, Ward w, GranularBuilder main, Network<Object> assigned) {
		super(intra);
		this.assigned = assigned;
		this.hcws = new ArrayList<HealthCareWorker>();
		for (HealthCareWorker o : hcws) {
				this.hcws.add(o);
			}
		
		this.ward = w;	
		this.main = main;
		this.stopped = true;
		this.schedule =repast.simphony.engine.environment.RunEnvironment.getInstance().getCurrentSchedule();
	}

	@Override
	public Object start() {
		stopped = false;
		double currTime = schedule.getTickCount();
		double eventTime = currTime + shiftRepeat;
		ScheduleParameters params = ScheduleParameters.createOneTime(eventTime);
		schedule.schedule(params, this, "assignEdges");
		return this;
	}
	
	public void assignEdges() {
		if (stopped == true) {
			return;
		}
		//System.out.println("hcws.size() = " + hcws.size());
		ArrayList<Patient> patients = ward.getPatients();
		if (hcws.size() == 0) {
			return;
		}
		//System.out.println("Clear nurse assignments");
		assigned.removeEdges();
		for (HealthCareWorker h : hcws) {
			h.setAssignedPatients(new ArrayList<Patient>());
		}
		
		for (Patient p : patients) {
			//System.out.println("for patient" + p.toString());
			int least = 1000;
			for (HealthCareWorker hc : hcws) {
				least = Math.min(least, assigned.getDegree(hc));
			}
			ArrayList<HealthCareWorker> leastUtilized = new ArrayList<HealthCareWorker>();
			for (HealthCareWorker hc : hcws) {
				if (hc.getAssignedPatientCount() == least){
					leastUtilized.add(hc);
				}
			}
			int rando = (int)(Math.random() * leastUtilized.size());
			HealthCareWorker target = leastUtilized.get(rando);
			target.getAssignedPatients().add(p);
			assigned.addEdge(target, p);
			//System.out.println("nurse " + target.toString() + " assigned to " + p.toString());
			main.event("ASSIGNMENT",target.toString() + " assigned to " + p.toString());
			
		}
		/* for (HealthCareWorker hcw : hcws) {
			System.out.println(hcw.toString());
			for (Patient p : hcw.getAssignedPatients()) {
				System.out.println("---- " + p.toString());
			}
		}
		*/
		start();
	}
	
	

	@Override
	public Object stop() {
		stopped = true;
		return null;
	}
	

}
