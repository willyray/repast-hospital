package processes

import java.awt.event.ActionEvent
import java.awt.event.ActionListener

import agents.Agent
import agents.Patient
import agents.HealthCareWorker
import bsh.This
import mainModel.GranularBuilder
import repast.simphony.dataLoader.ContextBuilder
import repast.simphony.space.graph.Network
import structure.Ward
import actionevents.*
import util.Chooser

class DoctorAssignmentManager extends Process implements ActionListener{
	def ArrayList<HealthCareWorker> hcws
	def Ward ward
	ContextBuilder<Object> main
	Network<Object> assigned

	public DoctorAssignmentManager(ArrayList<HealthCareWorker> hcws, Ward w, GranularBuilder main, Network<Object> assigned) {
		super(1)
		this.hcws = hcws
		this.ward = w
		this.main = main
		this.assigned = assigned
		
	}
	
	
	
	
	
	
	
	
	
	@Override
	public Object start() {
		return null;
	}

	@Override
	public Object stop() {
		return null;
	}









	@Override
	public void actionPerformed(ActionEvent e) {
		if (e instanceof PatientAdmitted ) {
			PatientAdmitted event = (PatientAdmitted)e
			Patient p = event.getPatient()
			
			HealthCareWorker target = Chooser.chooseOne(hcws)
			
			assigned.addEdge(target, p);
			main.event("ASSIGNMENT",target.toString() + " assigned to " + p.toString());
		}
		
		
		
	}
}
