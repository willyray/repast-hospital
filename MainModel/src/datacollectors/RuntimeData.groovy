package datacollectors

import repast.simphony.engine.schedule.ISchedule;

class RuntimeData {
	
	
	int visitCount
	double lastUpdate
	ISchedule schedule 
	
	public RuntimeData(ISchedule sc){
		schedule=sc
	}
	double getVisitsPerDay(){
		(double)visitCount/lastUpdate
	}
	
	double setVisitCount(int vc){
		this.visitCount=vc
		lastUpdate = schedule.getTickCount()
	}
		
}
