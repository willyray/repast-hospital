package structure;

import agents.Patient;

public interface Dischargeable {

	public void discharge(Patient p);
	
}
