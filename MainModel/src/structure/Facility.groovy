package structure

import repast.simphony.context.DefaultContext;
import java.util.ArrayList
import agents.Patient;

class Facility extends DefaultContext<Object>{
	ArrayList<Ward> wards
	ArrayList<Room> rooms
	ArrayList<Bed> beds

	int capacity
	
	
	Facility(){
		wards = new ArrayList<Ward>()
		rooms = new ArrayList<Room>()
		beds = new ArrayList<Bed>()
		
	}
	
	public void addWard(Ward w){
		wards.add(w)
		this.add(w)
	}
	
	List getEmptyBeds(){
		return beds.findAll { !it.isOccupied()}
	}
	
	int getVacancy(){
		int census = 0
		for(Ward w : wards){
			census+=(w.getCapacity()-w.getVacancy())
		}
		return capacity-census
	}
	
	ArrayList<Patient> getPatients(){
		ArrayList<Patient> ret = new ArrayList<Patient>()
		for (Ward w: wards){
			ret.addAll(w.getPatients())
		}
		return ret
	}
		
}
	

