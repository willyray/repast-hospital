package structure

import repast.simphony.engine.schedule.Schedule
import util.TimeUtils

class Surface {
	
	String name
	double surfaceArea
	double lastUpdate
	double load
	public static lambda = 1/5
	Schedule schedule
	
	public Surface(String name, double surf=1) {
		this.name=name
		this.surfaceArea=surf
		double lastUpdate = 0
		double load = 0
		schedule = repast.simphony.engine.environment.RunEnvironment.getInstance().getCurrentSchedule();
	}
	
	public void update() {
		update(TimeUtils.getSchedule().getTickCount())
	}
	
	public void update(double t) {
		double elapsed = t-lastUpdate
		double newLoad = load*Math.exp(-1*lambda*elapsed)
		load = newLoad
		lastUpdate = t
	}
	
	public getLoad() {
		update(schedule.getTickCount())
		return load
	}
	
	public addContam(double amount) {
		load += amount
		load = Math.min(load, 42000)
	}
	
	public clear() {
		load = 0
		lastUpdate = schedule.getTickCount()
	}
	
	
	
	
	
}
