package structure;

import agents.Agent;

public interface Visitable {

	public void visit(Agent visitor, Agent target);
	
	public void leave(Agent visitor);
	
}
