package structure

import java.util.ArrayList

import actionevents.RoomCleaningEvent;
import actionevents.RoomLeave
import actionevents.RoomVisit
import agents.HealthCareWorker
import agents.Patient
import agents.Agent;;;
import agents.AgentType


class Room extends Agent implements Visitable {
	int capacity=0
	String roomNumber
	boolean singlePatient
	boolean bathroom
	boolean sharedBathroom
	boolean negativePressure
	boolean isolation
	ArrayList<Bed> beds
	Ward ward
	ArrayList<Agent> hcwVisitors;
	ArrayList<Surface> nearZone;
	ArrayList<Surface> farZone;
 	
	
	public Room(String roomNumber, boolean singlePatient, boolean bathroom, boolean sharedBathroom,
			boolean negativePressure, boolean isolation, Ward ward) {
		super(AgentType.ROOM);
		beds = new ArrayList<Bed>()
		hcwVisitors = new ArrayList<HealthCareWorker>()
		this.roomNumber = roomNumber
		this.singlePatient = singlePatient
		this.bathroom = bathroom
		this.sharedBathroom = sharedBathroom
		this.negativePressure = negativePressure
		this.isolation = isolation
		this.ward = ward
		this.nearZone = new ArrayList<Surface>()
		this.farZone = new ArrayList<Surface>()
		
		
	}
	
	ArrayList<Patient> getPatients(){
		ArrayList<Patient> ret = new ArrayList<Patient>()
		for (Bed b : beds){
			if (b.isOccupied()){
				ret.add(b.patient)
			}
		} 
		return ret
	}
			
	int getOccupancy(){
		int occupantCount
		for (Bed b : beds){
			if (b.isOccupied()){
				occupantCount ++
			}
		}
		return occupantCount
	}
	
	int getVacancy(){
		return capacity-getOccupancy()
	}
	
	def addBed(Bed b){
		if (beds == null){
			beds = new ArrayList<Bed>();
		}
		beds.add(b)
		capacity++
	}

	@Override
	public void visit(Agent visitor, Agent target) {
		if (!hcwVisitors){
			hcwVisitors = new ArrayList<Agent>()
		}
		hcwVisitors.add(visitor)
		

		
	}

	@Override
	public void leave(Agent visitor) {
		assert hcwVisitors.remove(visitor)
		visitor.fireActionPerformed(new RoomLeave(visitor, this, this.getRoomNumber()))
		
	}
	
	def clean(){
		fireActionPerformed(new RoomCleaningEvent(this));
			}

	@Override
	public String toString() {
		return "Room " + roomNumber;
	}
	
	def Room(){
		
	}
	
	
}
