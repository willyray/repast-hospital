package structure

import org.codehaus.groovy.runtime.NullObject;

import agents.Patient;

class Bed {
	
	Patient patient
	Room room
	
	
	Bed(Room room){
		this.room = room
		
	}
	
	
	def setPatient(Patient p){
		patient = p
	}
	
	def isOccupied(){
		if (patient){
			return true
		}
		return false
	}
}
