package structure;

public interface Admittable {
	
	public void admit();

}
