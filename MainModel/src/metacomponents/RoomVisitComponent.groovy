package metacomponents

import agents.HealthCareWorker;
import structure.Room;

class RoomVisitComponent {
	

	static configureRoomVisit(){
		Room.metaClass.hcwVisitActions << {HealthCareWorker hcw ->
			//println("health care worker actions")
		}
		
		Room.getMetaClass().hcwLeaveActions << {HealthCareWorker hcw ->
			//leave actions
		}
	}
	
	
}
