package simple_adt

import org.apache.commons.math3.distribution.ExponentialDistribution

import agents.Patient;
import processes.Process
import repast.simphony.engine.schedule.ISchedulableAction;
import repast.simphony.engine.schedule.ScheduleParameters
import structure.Dischargeable

class Discharge extends Process {
	double meanLOS
	Dischargeable target
	
	Discharge(double meanLOS, Dischargeable target){
		super(meanLOS)
		distro = new ExponentialDistribution(meanLOS)
		this.target=target
	}
	
	def newPatient(Patient p){
		double currentTime = schedule.getTickCount()
		double timeToElapse = distro.sample()
		double eventTime = currentTime+timeToElapse
		ScheduleParameters schedParams = ScheduleParameters.createOneTime((eventTime))
		ISchedulableAction discharge = schedule.schedule(schedParams, target, "discharge", p)
	}
	
	def start(){
		
	}
	
	def stop(){
		
	}

}
