package actionevents

import java.awt.event.ActionEvent

import structure.Surface

class TouchSurfaceAction extends ActionEvent{
	
	Surface toucher,touchee
	
	public TouchSurfaceAction(Surface toucher, Surface touchee) {
		super(toucher, 10, "touchsurface")
		this.toucher = toucher
		this.touchee = touchee
		
	}
	
}
