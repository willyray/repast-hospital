package actionevents

import java.awt.event.ActionEvent

import agents.Patient

class PatientDischarged extends ActionEvent{
	Patient patient
	
	PatientDischarged(Patient p){
		super(p, 2, "Patient Discharged")
		patient = p
	}
	

}
