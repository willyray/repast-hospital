package actionevents

import java.awt.event.ActionEvent

import agents.Patient

class PatientAdmitted extends ActionEvent {
	Patient patient
	
	PatientAdmitted(Patient p){
		super((Object)p, 0, "Patient Admitted")
		patient = p
	}

}
