package actionevents

import java.awt.event.ActionEvent

import agents.HealthCareWorker
import agents.Patient
import granular.behavior.VisitType
import agents.Agent
import structure.Room

class RoomVisit extends ActionEvent {
	HealthCareWorker hcw
	Room room
	Patient patient
	VisitType visitType
	
	RoomVisit(Agent hcw, Room room, Patient p, VisitType type, String message){
		super(hcw,4,message)
		this.hcw = (HealthCareWorker)hcw
		this.room=room
		this.patient=p	
		this.visitType = type
	}
}
