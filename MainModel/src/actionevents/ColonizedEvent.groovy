package actionevents

import java.awt.event.ActionEvent

import agents.Patient

class ColonizedEvent extends ActionEvent{
	public Patient patient
	public double load
	
	public ColonizedEvent(Patient p, double load) {
		super((Object)p, 16, "colonized")
		this.patient = p
		this.load = load
	}
	
}
