package actionevents

import java.awt.event.ActionEvent
import structure.Room

import agents.HealthCareWorker;

class RoomLeave extends ActionEvent {
	HealthCareWorker healthCareWorker
	Room room
	
	
	RoomLeave(HealthCareWorker hcw, Room room, String message){
		super(hcw, 7, message)
		healthCareWorker = hcw
		room = room
	}
	
	
}
