package actionevents

import java.awt.event.ActionEvent
import structure.Room

class RoomCleaningEvent extends ActionEvent {
	Room room

	public RoomCleaningEvent(Room source) {
		super((Object)source, 5, "Room Cleaning");
		room = source;
		
	}

	

}
